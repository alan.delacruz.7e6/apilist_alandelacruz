package com.example.apilist_alandelacruz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist_alandelacruz.API.infogameItem
import com.example.apilist_alandelacruz.databinding.FragmentFavoritegameBinding
import com.example.apilist_alandelacruz.room.GameDataBase
import com.example.apilist_alandelacruz.room.GameEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope.coroutineContext
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class favoritegameFragment : Fragment(), OnClickListener {
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    lateinit var binding: FragmentFavoritegameBinding

    val database: GameDataBase by lazy { GameDataBase.getDatabase(requireActivity().applicationContext) }

    lateinit var list: MutableLiveData<List<GameEntity>?>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritegameBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // binding.

        linearLayoutManager = LinearLayoutManager(context)


        list= MutableLiveData(null)
        CoroutineScope(Dispatchers.IO).launch {
            list.postValue(database.gameDao().getAll())
        }



        list.observe(viewLifecycleOwner) {
            if (it != null){

                val myAdapter = FavoriteAdapter(it, this@favoritegameFragment)

                binding.recyclerView.apply {
                    adapter = myAdapter
                    layoutManager = linearLayoutManager
                }

            }



        }


    }


    override fun onClick(game: infogameItem) {
        val action =
            favoritegameFragmentDirections.actionFavoritegameFragmentToDetailFragment(game.id)

        findNavController().navigate(action)
    }

    override fun onClick(game: GameEntity) {
        val action =
            favoritegameFragmentDirections.actionFavoritegameFragmentToDetailFragment(game.id.toInt())

        findNavController().navigate(action)
    }
}