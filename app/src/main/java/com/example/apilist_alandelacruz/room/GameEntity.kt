package com.example.apilist_alandelacruz.room

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.versionedparcelable.VersionedParcelize


@VersionedParcelize

@Entity(tableName = "GameEntity")
data class GameEntity(
    @PrimaryKey(autoGenerate = false) var id: Long,
    var name:String,
    var descriptiongame: String,
    val thumbnail: String,
    var plataformgame: String)
