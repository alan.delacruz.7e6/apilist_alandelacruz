package com.example.apilist_alandelacruz.room
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface GameDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(game: GameEntity)



    @Delete
    fun deleteGame(gameEntity: GameEntity)

    @Query("SELECT EXISTS(SELECT * FROM GameEntity WHERE id = :id)")
    fun artExist(id : Long) : Boolean

    @Query("SELECT * FROM GameEntity")
     fun getAll(): List<GameEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(allGames: List<GameEntity>)

    @Update
    fun update(game: GameEntity)


}