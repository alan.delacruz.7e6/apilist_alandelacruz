package com.example.apilist_alandelacruz.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [GameEntity::class], version = 1, exportSchema = false)
abstract class GameDataBase : RoomDatabase() {
    abstract fun gameDao(): GameDao


    companion object {

        @Volatile
        private var INSTANCE: GameDataBase? = null

        fun getDatabase(context: Context): GameDataBase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    GameDataBase::class.java,
                    "GameDataBase"
                )
                    .build()
                INSTANCE = instance

                instance
            }
        }

    }
}
