package com.example.apilist_alandelacruz.room

import android.app.Application
import androidx.room.Room

class GameApplication : Application() {
    companion object {
        lateinit var dataBase: GameDataBase
    }

    override fun onCreate() {
        super.onCreate()
        dataBase = Room.databaseBuilder(this, GameDataBase::class.java, "GameDataBase").build()
    }
}