package com.example.apilist_alandelacruz.API

import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiInterface {
    companion object {
        val BASE_URL = "https://www.freetogame.com"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

    @GET("/api/games")
    suspend fun getGames(): Response<infogame>
    @GET("/api/games/{id}")
    suspend fun getGame(@Path("id") gameId: Int): Response<infogame>

}