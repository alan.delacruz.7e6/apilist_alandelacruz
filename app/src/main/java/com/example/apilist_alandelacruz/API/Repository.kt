package com.example.apilist_alandelacruz.API

class Repository {
    val apiInterface = ApiInterface.create()
    suspend fun getGames() = apiInterface.getGames()
}