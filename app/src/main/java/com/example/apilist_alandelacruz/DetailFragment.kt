package com.example.apilist_alandelacruz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apilist_alandelacruz.ViewModel.QuotesTagsViewModel
import com.example.apilist_alandelacruz.databinding.FragmentDetailBinding
import com.example.apilist_alandelacruz.room.GameApplication
import com.example.apilist_alandelacruz.room.GameDataBase
import com.example.apilist_alandelacruz.room.GameEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class DetailFragment : Fragment() {
    lateinit var binding: FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[QuotesTagsViewModel::class.java]
        val id = arguments?.getInt("id")


        val favorite = arguments?.getBoolean("favorite")

        val game = viewModel.gameData(id!!)

        Glide.with(requireContext())
            .load(game?.thumbnail)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.gameimg)
        binding.titlegame.text = game?.title
        binding.genregame.text = game?.genre
        binding.platformgame.text = game?.platform
        binding.descriptiongame.text = game?.short_description

        if (favorite == true) binding.favoritegame.alpha = 1F
        else binding.favoritegame.alpha = 0.5F

        binding.favoritegame.setOnClickListener{
            val gamedetail = GameEntity(id =game?.id.toString().toLong(),name = game?.title.toString(), descriptiongame = game?.short_description.toString(), plataformgame = game?.platform.toString(), thumbnail = game?.thumbnail.toString())
            if (binding.favoritegame.alpha == 0.5F){
                binding.favoritegame.alpha = 1.0F
                game?.favorite = true
                CoroutineScope(Dispatchers.IO).launch {
                    GameApplication.dataBase.gameDao().insert(gamedetail)
                }
            }
            else{
                binding.favoritegame.alpha = 0.5F
                game?.favorite = false
                CoroutineScope(Dispatchers.IO).launch {
                    GameApplication.dataBase.gameDao().deleteGame(gamedetail)
                }
            }
        }
    }
}