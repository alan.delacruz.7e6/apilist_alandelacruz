package com.example.apilist_alandelacruz

import com.example.apilist_alandelacruz.API.infogameItem
import com.example.apilist_alandelacruz.room.GameEntity

interface OnClickListener {
    fun onClick(game: infogameItem)
    fun onClick(game: GameEntity)

}
