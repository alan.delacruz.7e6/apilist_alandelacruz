package com.example.apilist_alandelacruz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentContainer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist_alandelacruz.API.infogameItem
import com.example.apilist_alandelacruz.ViewModel.QuotesTagsViewModel
import com.example.apilist_alandelacruz.databinding.FragmentRecyclerviewBinding
import com.example.apilist_alandelacruz.room.GameEntity

class RecyclerViewFragment : Fragment(), OnClickListener {
    private lateinit var gameAdapter: GameAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerviewBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRecyclerviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun getGames(): MutableList<infogameItem> {
        val games = mutableListOf<infogameItem>()

        return games
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gameAdapter = GameAdapter(getGames(), this)
        linearLayoutManager = LinearLayoutManager(context)
        val viewModel = ViewModelProvider(requireActivity())[QuotesTagsViewModel::class.java]


        viewModel.data.observe(viewLifecycleOwner) {
            if (it != null) {
                setUpRecycler(it)
            } else {
                binding.recyclerView.apply {
                    setHasFixedSize(true) //Optimitza el rendiment de l’app
                    layoutManager = linearLayoutManager
                    adapter = gameAdapter
                }
            }

        }


    }

    fun setUpRecycler(resultList: List<infogameItem>) {
        val myAdapter = GameAdapter(resultList, this)
        binding.recyclerView.apply {
            adapter = myAdapter
            layoutManager = linearLayoutManager
        }
    }

    override fun onClick(game: infogameItem) {
        val action = RecyclerViewFragmentDirections.actionRecyclerViewToDetailFragment(game.id)
        findNavController().navigate(action)
    }

    override fun onClick(game: GameEntity) {
        val action = RecyclerViewFragmentDirections.actionRecyclerViewToDetailFragment(game.id.toInt())
        findNavController().navigate(action)
    }


}