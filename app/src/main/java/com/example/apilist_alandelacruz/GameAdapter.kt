package com.example.apilist_alandelacruz

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.apilist_alandelacruz.API.infogameItem
import com.example.apilist_alandelacruz.databinding.ItemUserBinding

class GameAdapter(
    private val games: List<infogameItem>,
    private val listener: RecyclerViewFragment
) :
    RecyclerView.Adapter<GameAdapter.ViewHolder>() {



    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemUserBinding.bind(view)
        fun setListener(game: infogameItem) {
            binding.root.setOnClickListener {
                listener.onClick(game)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return games.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val game = games[position]
        with(holder) {
            setListener(game)
            binding.gametitle.text = game.title
            binding.gameid.text = game.id.toString()
            Glide.with(context)
                .load(game.thumbnail)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.imggame)
        }
    }

    //grijibjsvs
}

