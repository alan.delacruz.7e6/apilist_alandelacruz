package com.example.apilist_alandelacruz.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.apilist_alandelacruz.API.Repository
import com.example.apilist_alandelacruz.API.infogame
import com.example.apilist_alandelacruz.API.infogameItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class QuotesTagsViewModel(): ViewModel() {
    private val repository = Repository()
    var data = MutableLiveData<infogame?>()

    init {
        fetchData()
    }
    private fun fetchData(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getGames()
            println(response)
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun gameData(id: Int): infogameItem?{
        var i = 0
        var item:infogameItem? = null
        while (i in data.value!!.indices&&item==null){
            if(id == data.value!![i].id) item = data.value!![i]
            i++
        }
        return item
    }

}